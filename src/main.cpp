#include "cuda_utilities.h"
#if CUDA_FOUND
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/generate.h>
#include <thrust/reduce.h>
#include <thrust/functional.h>
#endif
#include <algorithm>
#include <cstdlib>

int main(int argc, char* argv[])
{
  cuda_set_device(0);

#if CUDA_FOUND
  std::cout << "Prepare parallel execution ...\n";
  std::cout << "CUDA device #0 joins us ...\n";

  // Now do a simple Thrust test
  std::cout << "Test Thrust support ...\n";

  // generate random data serially
  thrust::host_vector<int> h_vec(100);
  std::generate(h_vec.begin(), h_vec.end(), rand);

  // transfer to device and compute sum
  thrust::device_vector<int> d_vec = h_vec;
  int x = thrust::reduce(d_vec.begin(), d_vec.end(), 0, thrust::plus<int>());
  std::cout << "Sum of 100 random numbers = " << x << '\n';

#endif

  return EXIT_SUCCESS;
}
