if (NOT CUDA_DISABLED)

  find_package(CUDAToolkit QUIET)
  set(CUDA_FOUND ${CUDAToolkit_FOUND})

  if (CUDA_FOUND)

    # enable CUDA as a language
    message(STATUS "This build will use CUDA support.")
    if(NOT DEFINED CMAKE_CUDA_ARCHITECTURES)
      set(CMAKE_CUDA_ARCHITECTURES 70)
    endif()
    enable_language(CUDA)

    # Now enable Thrust, first check if a config file is available ...
    find_package(Thrust QUIET CONFIG)
    if (Thrust_FOUND)
      message(STATUS "Found the Thrust package via config mode.")
      thrust_create_target(Thrust)
      # Now we can use target_link_libraries with "Thrust".
    else()
      # ... otherwise fall back to our "FindThrust.cmake" module file.
      message(STATUS "Thrust package not found via config mode, will try find-module heuristics next.")
      find_package(Thrust MODULE REQUIRED)
      message(STATUS "Thrust package found via heuristics with include dir: ${THRUST_INCLUDE_DIR}.")
      # Now we can use target_include_directories with "${THRUST_INCLUDE_DIR}".
    endif()

    # Enable separable compilation, e.g. to use the "extern" keyword for
    # __constant__ variables.
    set(CMAKE_CUDA_SEPARABLE_COMPILATION ON)

    # Enable experimental relexad constexpr to compile classes that use
    # constexpr operator[] calls to std::array, such as vec4.
    set(CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} --expt-relaxed-constexpr -lineinfo")

  else()

    message(STATUS "This build will not use CUDA support (not found).")

  endif()

else()

  message(STATUS "This build will not use CUDA support (disabled by user).")
  set(CUDA_FOUND 0)

endif()
